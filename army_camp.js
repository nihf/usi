var requestManager = Sys.WebForms.PageRequestManager.getInstance();
requestManager.add_beginRequest(BeginRequestHandler);
requestManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {
    // Add your logic here
};

function EndRequestHandler(sender, args) {
$("<h3 style='font-size: large; color: red;'><i>Please note: Students receiving a scholarship MUST commit to attending Camp every day. Any child not at camp on the first day will lose their place to the next child on the wait list.</i></h3>").insertBefore('div[id*="ctl00_cphBody_ctl00_cphBody_TermsAndConditions_LayoutTable"]' );
$("<h2>TERMS & CONDITIONS:</h2><h3>Acceptable Behavior Policy:</h3><p>To ensure a safe and fun environment for all, children are expected to behave in an acceptable manner and use appropriate language at all times. It is important to remember that there are no refunds if a child is asked to leave the program due to unacceptable behavior.</p><h3>Accommodations:</h3><p>The National Inventors Hall of Fame programming presents children with fun, hands-on challenges that encourage creative problem solving, teamwork, entrepreneurship, and innovation. Please keep in mind locations hosting summer/after-hours programs do not have the same personnel/tools available as during the school year. *Nurses, special education assistance, aides, etc., are not on site unless a parent arranges an approved accommodation beforehand based on their individual child needs.</p> <p>If your child’s needs are <u>not</u> self-managed, please contact us at 800-968-4332 to discuss accommodations a minimum of <b>4 weeks</b> prior to the start date of your child’s program. If 1:1 assistance is requested, we will provide you with a form to submit any necessary information. All reasonable accommodation requests will be reviewed to ensure safety and program integrity.</p><h3>Photography Release:</h3><p>By selecting Yes, you authorize Club Invention/Camp Invention/Invention Project/Invention Playground, corporate and government sponsors and affiliates, to obtain, store, publish and/or use (without payment) any photographs, slides, sound and/or video recordings made of your child for public relations, marketing/advertising and/or internal training purposes.</p><h3>Emergency Treatment Authorization:</h3><p> You hereby authorize the diagnosis and treatment by a qualified and licensed medical professional, of your child, should a medical emergency occur, which the attending medical professional believes requires immediate attention to prevent further endangerment of the minor’s life, physical disfigurement or impairment, or undue pain, suffering or discomfort, if delayed. Permission is granted to the attending physician to proceed with any examination, diagnosis and medical or minor surgical or other treatment. In the event of a medical emergency you understand that every attempt will be made by the attending physician to contact you in the most expeditious way possible. The authorization is granted only after a reasonable effort has been made to reach you. Permission is also granted to the National Inventors Hall of Fame, Inc. and its affiliates to provide emergency treatment prior to the child’s admission to the medical facility. This release is authorized for the duration of the registered session. This release is authorized and executed of your own free will, with the sole purpose of authorizing medical treatment under emergency circumstances, for the protection of life and limb of the named minor child, in your absence.</p><h3>Liability Waiver:</h3><p>On your own behalf, and as parent or guardian, you acknowledge and agree that there is the possibility of physical injury or loss associated with your child’s participation in the program and hereby release, discharge the National Inventors Hall of Fame, Inc., its affiliated organizations, employees and associated personnel including the owners of the program facility against any and all claims, liabilities and/or damages as a result of your child’s participation in the program.</p><h3>CONFIRMATION:</h3><p>By registering your child you have read and agreed to the Terms & Conditions of the program and is required for your child to participate.</p>").insertBefore('div[id*="ctl00_cphBody_ctl00_cphBody_TermsAndConditions_LayoutTable"]' );
$("<h3 style='color: red;'>No payment needed, this registration received a scholarship donation!</h3>").insertBefore('div[id*="ctl00_cphBody_tblAddAnotherRegistrantButtons-"]');

$("legend:contains('Who')").parent('fieldset').hide();
//$("div[id*='ctl00_cphBody_CopyItems-']").hide();
  $('<span id="donationDollar">\$</span>').insertBefore('input[class*="ux-reg-input-numeric"]');
  var modal = $("div[id*='ctl00_cphBody_PopUpDivpopUpAddRegistrant-']");
    
  if (typeof(modal) != 'undefined' && modal != null)
  {
             $("input[id*='ctl00_cphBody_CopyItemsFromRegistrant-']").prop("checked", false);
    
$("input[id*='ctl00_cphBody_DoNotCopy-']").prop("checked", true);
  }
  
  $("#ctl00_cphBody_cboPaymentPlan_HTMLSelect").change(function () {
  $("#ctl00_cphBody_btnUpdateCart").click();
});

  $("#ctl00_cphBody_lblAmountDue").text('Scholarship Amount: ');
  
};

window.onload = function() {
  $('head').append("<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-KHLK4F');</script><!-- End Google Tag Manager -->");
$('body').append('<!-- Google Tag Manager (noscript) --><NOSCRIPT><IFRAME SRC="https://www.googletagmanager.com/ns.html?id=GTM-KHLK4F" HEIGHT="0" WIDTH="0" STYLE="display:none; visibility:hidden;"></IFRAME></NOSCRIPT><!-- End Google Tag Manager (noscript) -->');
  $("<h3 style='color: red;'>No payment needed, this registration received a scholarship donation!</h3>").insertBefore('div[id*="ctl00_cphBody_tblAddAnotherRegistrantButtons-"]');
$("#ctl00_cphBody_cboPaymentPlan_HTMLSelect").change(function () {
  $("#ctl00_cphBody_btnUpdateCart").click();
});
var progress = $('span[class*="ux-reg-progress-current"]').html();
var complete = $('div[id*="ctl00_cphBody_objMessageUpdatePanel"]').html();
var step1 = false;
var step2 = false
if(progress != (null || undefined)) {
  if(progress.indexOf('Step 1') !== -1){
    var step1 = true
  }
  if(progress.indexOf('Step 2') !== -1){
    var step2 = true
  }
  //var step1 = progress.includes("Step 1");
  //var step2 = progress.includes("Step 2");
}
if(complete != (null || undefined)) {
  if(complete.indexOf('Thank you!') !== -1){
    var step3 = true
  }
  //var step3 = complete.includes("Thank you!");
  //alert(step3);
}

  if(step1) {
    //alert("step 1");
    dataLayer.push({ event: "registration_step1" });
  }
  if(step2) {
    //alert("step 2");
    dataLayer.push({ event: "registration_step2" });
  }
  if(step3) {
    //alert("step 3");
    dataLayer.push({ event: "confirmation_page" });
  }
$("#ctl00_cphBody_lblAmountDue").text('Scholarship Amount: ');
$("#ctl00_cphBody_ctl00_cphBody_ctl06_LayoutTable").insertBefore("#ctl00_cphBody_ctl00_cphBody_TermsAndConditions_LayoutTable");
$("legend:contains('Who')").parent('fieldset').hide();
  //$("div[id*='ctl00_cphBody_CopyItems-'").hide();
var newHtml = $('label[for*="ctl00_cphBody_NEW_RadioButton"]').html();
  if (newHtml != undefined) {
newHtml = newHtml.replace(/[\$.-]/g, "");
newHtml = newHtml.replace(/[0-9]+/, "");
$('label[for*="ctl00_cphBody_NEW_RadioButton"]').html(newHtml);
  }

var alumniHtml = $('label[for*="ctl00_cphBody_ALUMNI_RadioButton"]').html();
  if (alumniHtml != undefined) {
alumniHtml = alumniHtml.replace(/[\$.-]/g, "");
alumniHtml = alumniHtml.replace(/[0-9]+/, "");
$('label[for*="ctl00_cphBody_ALUMNI_RadioButton"]').html(alumniHtml);
  }
var LITHtml = $('label[for*="ctl00_cphBody_LIT_RadioButton"]').html();
if (LITHtml != undefined) {  
  LITHtml = LITHtml.replace(/[\$.-]/g, "");
  LITHtml = LITHtml.replace(/[0-9]+/, "");
  $('label[for*="ctl00_cphBody_LIT_RadioButton"]').html(LITHtml);
}
var PTMNew = $('label[for*="ctl00_cphBody_PTM-NEW_RadioButton"]').html();
if (PTMNew != undefined) {  
  PTMNew = PTMNew.replace(/[\$.-]/g, "");
  PTMNew = PTMNew.replace(/[0-9]+/, "");
  $('label[for*="ctl00_cphBody_PTM-NEW_RadioButton"]').html(PTMNew);
}
var PTMRet = $('label[for*="ctl00_cphBody_PTM-ALUMNI_RadioButton"]').html();
if (PTMRet != undefined) {  
  PTMRet = PTMRet.replace(/[\$.-]/g, "");
  PTMRet = PTMRet.replace(/[0-9]+/, "");
  $('label[for*="ctl00_cphBody_PTM-ALUMNI_RadioButton"]').html(PTMRet);
}



$("#doggo").css("table-layout", "fixed");
$( "table" ).addClass(function( index ) {
  return "item-" + index;
});
$('img[alt*="Loading..."]').addClass("loading");
 
$("<p style='margin-bottom:15px; padding-left:14px;' class='SectionLayoutTableInfo'>For any child needs that are <b>NOT</b> self-managed, please see our <a href='http://www.invent.org/terms-conditions/' target='_blank'>Terms & Conditions</a></p>" ).insertBefore( 'label[id*="_RF_TXT_08_"]' );

  $('<span id="donationDollar">\$</span>').insertBefore('input[class*="ux-reg-input-numeric"]');
  
  var checkExist = window.setInterval(function() {   

    var modal = $("div[id*='ctl00_cphBody_PopUpDivpopUpAddRegistrant-']");
    
  if (typeof(modal) != 'undefined' && modal != null)
  {
             $("input[id*='ctl00_cphBody_CopyItemsFromRegistrant-']").prop("checked", false);
    
$("input[id*='ctl00_cphBody_DoNotCopy-']").prop("checked", true);
    
      clearInterval(checkExist);
  }
}, 100); // check every 100ms
 $("<h3 style='font-size: large; color: red;'><i>Please note: Students receiving a scholarship MUST commit to attending Camp every day. Any child not at camp on the first day will lose their place to the next child on the wait list.</i></h3>").insertBefore('div[id*="ctl00_cphBody_ctl00_cphBody_TermsAndConditions_LayoutTable"]' );
  $("<h2>TERMS & CONDITIONS:</h2><h3>Acceptable Behavior Policy:</h3><p>To ensure a safe and fun environment for all, children are expected to behave in an acceptable manner and use appropriate language at all times. It is important to remember that there are no refunds if a child is asked to leave the program due to unacceptable behavior.</p><h3>Accommodations:</h3><p>The National Inventors Hall of Fame programming presents children with fun, hands-on challenges that encourage creative problem solving, teamwork, entrepreneurship, and innovation. Please keep in mind locations hosting summer/after-hours programs do not have the same personnel/tools available as during the school year. *Nurses, special education assistance, aides, etc., are not on site unless a parent arranges an approved accommodation beforehand based on their individual child needs.</p> <p>If your child’s needs are <u>not</u> self-managed, please contact us at 800-968-4332 to discuss accommodations a minimum of <b>4 weeks</b> prior to the start date of your child’s program. If 1:1 assistance is requested, we will provide you with a form to submit any necessary information. All reasonable accommodation requests will be reviewed to ensure safety and program integrity.</p><h3>Photography Release:</h3><p>By selecting Yes, you authorize Club Invention/Camp Invention/Invention Project/Invention Playground, corporate and government sponsors and affiliates, to obtain, store, publish and/or use (without payment) any photographs, slides, sound and/or video recordings made of your child for public relations, marketing/advertising and/or internal training purposes.</p><h3>Emergency Treatment Authorization:</h3><p> You hereby authorize the diagnosis and treatment by a qualified and licensed medical professional, of your child, should a medical emergency occur, which the attending medical professional believes requires immediate attention to prevent further endangerment of the minor’s life, physical disfigurement or impairment, or undue pain, suffering or discomfort, if delayed. Permission is granted to the attending physician to proceed with any examination, diagnosis and medical or minor surgical or other treatment. In the event of a medical emergency you understand that every attempt will be made by the attending physician to contact you in the most expeditious way possible. The authorization is granted only after a reasonable effort has been made to reach you. Permission is also granted to the National Inventors Hall of Fame, Inc. and its affiliates to provide emergency treatment prior to the child’s admission to the medical facility. This release is authorized for the duration of the registered session. This release is authorized and executed of your own free will, with the sole purpose of authorizing medical treatment under emergency circumstances, for the protection of life and limb of the named minor child, in your absence.</p><h3>Liability Waiver:</h3><p>On your own behalf, and as parent or guardian, you acknowledge and agree that there is the possibility of physical injury or loss associated with your child’s participation in the program and hereby release, discharge the National Inventors Hall of Fame, Inc., its affiliated organizations, employees and associated personnel including the owners of the program facility against any and all claims, liabilities and/or damages as a result of your child’s participation in the program.</p><h3>CONFIRMATION:</h3><p>By registering your child you have read and agreed to the Terms & Conditions of the program and is required for your child to participate.</p>").insertBefore('div[id*="ctl00_cphBody_ctl00_cphBody_TermsAndConditions_LayoutTable"]' );
  
}